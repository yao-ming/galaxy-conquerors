import inspect
import traceback

from enum import Enum
from django.conf import settings
from django.core.urlresolvers import reverse


from HTMLParser import HTMLParser  # HTML unescaping via standard lib
htmlparser = HTMLParser()


def html_entities_to_unicode(text):
    """Convert HTML entities to unicode.  For example '&amp;' becomes '&'."""
    return htmlparser.unescape(text)


def nice_reverse(url_name, *args):
    return reverse(url_name, args=args)


def log_errors(func):
    """
    Stupid json_view decorator doesn't log exceptions properly so
    I need to put this decorator always behind @json_view

    TODO: find out why this happens. Maybe is a configuration error in our application

    """
    if not settings.DEBUG:
        return func

    def wrapper(request, *args, **kwargs):
        try:
            return func(request, *args, **kwargs)
        except:
            print traceback.format_exc()
    return wrapper


class ChoiceEnum(Enum):

    @classmethod
    def choices(cls):
        # get all members of the class
        members = inspect.getmembers(cls, lambda m: not(inspect.isroutine(m)))
        # filter down to just properties
        props = [m for m in members if not(m[0][:2] == '__')]
        # format into django choice tuple
        choices = tuple([(str(p[1].value), p[0]) for p in props])
        return choices