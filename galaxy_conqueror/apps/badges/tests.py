# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.test import TestCase, override_settings
from django.utils.text import slugify
from badges.api_proxy import proxy
from badges.models import Achievement
from users.models import User
from game.models import AlienHunted, GalaxyCandidate, Vote
from utils import html_entities_to_unicode


def create_test_user(first_name, last_name):
    user = User()
    user.username = first_name.lower() + '.' + last_name.lower()
    user.first_name = first_name
    user.last_name = last_name
    user.email = user.username + '@test.com'
    user.set_password('qwerty')
    user.save()
    return user


@override_settings(BADGES_API_APP_ID='galaxy_conqueror_test')
class BadgesApiTestCase(TestCase):

    def setUp(self):
        self.proxy = proxy
        self.achievements = self.proxy.get_achievements()
        self.badges = self.proxy.get_badges()
        self.proxy.reset()

    def test_achievements_amount(self):
        self.assertEqual(len(self.achievements), 10)

    def test_image_url(self):
        pattern = "https://cientopolis.lifia.info.unlp.edu.ar/galaxy-conqueror/badges/images/%s.png"
        for a in self.achievements:
            self.assertEqual(a['imageUrl'], pattern % slugify(html_entities_to_unicode(a['name'])))

    def test_criteria_url(self):
        pattern = "https://cientopolis.lifia.info.unlp.edu.ar/galaxy-conqueror/badges/%s"
        for a in self.achievements:
            self.assertEqual(a['criteriaUrl'], pattern % slugify(html_entities_to_unicode(a['name'])))

    def test_criteria_array_length(self):
        for a in self.achievements:
            self.assertEqual(len(a['criteria']), 5)

    def test_criteria_fields(self):
        criteria_descriptions = [
            "points earned required",
            "aliens hunted required",
            "votes required",
            "galaxies marked required",
            "galaxies discovered required",
        ]
        for a in self.achievements:
            for i, c in enumerate(a['criteria']):
                self.assertIn('id', c)
                self.assertIn('description', c)
                self.assertIn('required', c)
                self.assertIn('note', c)
                self.assertEqual(c['description'], criteria_descriptions[i])

    def test_description(self):
        for a in self.achievements:
            self.assertIn('description', a)

    def test_names(self):
        names = [html_entities_to_unicode(a['name']) for a in self.achievements]
        names.sort()
        self.assertEqual(names, [
            "Cadete espacial",
            "Conquistador galáctico",
            "Corsario galáctico",
            "Cosmonauta destacado",
            "Emperador galáctico",
            "Escudero estelar",
            "Explorador estelar de élite",
            "Explorador habilidoso",
            "Explorador principiante",
            "Observador nato",
        ])

    def test_id_badge_classes_uniqueness(self):
        ids = [a['id_badge_class'] for a in self.achievements]
        self.assertEqual(len(set(ids)), 10)

    def test_there_are_no_badges(self):
        self.assertEqual(len(self.badges), 0)
        self.assertEqual(self.proxy.badge_is_own_by_user('1', 'fake@mail.com'), {
            'issuedOn': None,
        })

    def count_user_achievements(self, email):
        return len(self.proxy.get_achievements_by_user(email)[email])

    def test_achievements_acquisition(self):
        email = 'batman@test.com'
        id_ = self.achievements[0]['id_badge_class']

        self.assertIsNone(self.proxy.achievement_is_own_by_user(id_, email)['issuedOn'])
        self.assertEqual(self.count_user_achievements(email), 0)

        self.proxy.create_achievement_acquisition(id_, email)

        self.assertEqual(self.count_user_achievements(email), 1)
        self.assertIsNotNone(self.proxy.achievement_is_own_by_user(id_, email)['issuedOn'])

        self.proxy.create_achievement_acquisition(id_, email)

        self.assertEqual(self.count_user_achievements(email), 1)
        self.assertIsNotNone(self.proxy.achievement_is_own_by_user(id_, email)['issuedOn'])

    def test_achievements_by_user(self):
        email = 'batman@test.com'
        id_1 = self.achievements[0]['id_badge_class']
        id_2 = self.achievements[1]['id_badge_class']

        self.assertEqual(self.count_user_achievements(email), 0)

        self.proxy.create_achievement_acquisition(id_1, email)
        self.proxy.create_achievement_acquisition(id_2, email)

        user_achievements = self.proxy.get_achievements_by_user(email)

        self.assertEqual(self.count_user_achievements(email), 2)

        self.assertEqual(len(user_achievements), 1)
        self.assertIn(email, user_achievements)
        ids = set()
        for a in user_achievements[email]:
            ids.add(a['id_badge_class'])
            self.assertIn('id_badge_class', a)
            self.assertIn('name', a)
            self.assertIn('issuedOn', a)
            self.assertEqual(len(a), 3)
        self.assertEqual(ids, {id_1, id_2})


@override_settings(BADGES_API_APP_ID='galaxy_conqueror_test')
class AchievementsTestCase(TestCase):

    def setUp(self):
        self.proxy = proxy
        self.proxy.reset()
        Achievement.update()
        self.user = create_test_user('Jimmy', 'Test')
        self.other_user = create_test_user('Johnny', 'Test')
        self.admin = create_test_user('Admin', 'Test')
        self.admin.is_staff = True
        self.admin.save()
        self.achs = sorted(Achievement.all())

    def assert_level(self, level):
        if level > 1:
            self.assertFalse(self.achs[level - 1].is_deserved_by(self.user))
            self.assertFalse(self.achs[level - 1].is_own_by(self.user))
        else:
            self.assertTrue(self.achs[level - 1].is_deserved_by(self.user))
            self.assertTrue(self.achs[level - 1].is_own_by(self.user))
        self.assertEqual(len(self.user.get_achievements()), 1)

        for i in xrange(self.achs[level - 1].votes_required):
            self.vote(i)

        for i in xrange(self.achs[level - 1].galaxies_marked_required):
            self.mark_galaxy(i)

        for i in xrange(self.achs[level - 1].galaxies_discovered_required):
            self.conquer_galaxy(i)

        for i in xrange(self.achs[level - 1].aliens_hunted_required):
            self.hunt(i)

        self.earn_points(self.achs[level - 1].points_earned_required)

        self.assertTrue(self.achs[level - 1].is_deserved_by(self.user))
        self.assertTrue(self.achs[level - 1].is_own_by(self.user))
        self.assertEqual(len(self.user.get_achievements()), level)

    def test_level_1(self):
        self.assert_level(1)

    def test_level_2(self):
        self.assert_level(2)

    def test_level_3(self):
        self.assert_level(3)

    def test_level_4(self):
        self.assert_level(4)

    def test_level_5(self):
        self.assert_level(5)

    def test_level_6(self):
        self.assert_level(6)

    def test_level_7(self):
        self.assert_level(7)

    def test_level_8(self):
        self.assert_level(8)

    def test_level_9(self):
        self.assert_level(9)

    def test_level_10(self):
        self.assert_level(10)

    def vote(self, i):
        galaxy = GalaxyCandidate.objects.create(owner=self.other_user, x=i * 1000, y=i * 1000)
        Vote.objects.create(
            galaxy_candidate=galaxy,
            owner=self.user,
            is_positive=i % 2 == 0,
        )

    def hunt(self, i):
        AlienHunted.objects.create(hunter=self.user, x=i * 1000, y=i * 1000)

    def mark_galaxy(self, i):
        GalaxyCandidate.objects.create(owner=self.user, x=i * 1000, y=i * 1000)

    def conquer_galaxy(self, i):
        galaxy = GalaxyCandidate.objects.create(owner=self.user, x=i * 1000, y=i * 1000)
        galaxy.confirm(self.admin)

    def earn_points(self, score):
        self.user.score = score
        self.user.save()

    def test_order(self):
        # chequeo que los niveles tengan sentido y sean incrementales
        # otra manera de verlo es que el orden en que se puede obtener debe ser determinístico,
        # es decir que hay un solo orden posible de obtención de insignias
        # de esta forma, dadas dos insignias cualquiera, sus valores requeridos deben ser todos
        # menores o iguales entre sí o todos mayores o iguales entre sí
        # un ejemplo del caso no deseado sería el siguiente:
        # para una insignia cualquiera A se requiren, entre otras cosas, 10 galaxias y 0 votos
        # mientras que para otras se requiren 0 galaxias y 10 votos
        # el orden en que se obtengan no será fijo y dependerá de el orden en que los usuarios hagan
        # las cosas
        for a1 in self.achs:
            for a2 in self.achs:
                attrs = zip(a1._criteria_tuple(), a2._criteria_tuple())
                self.assertTrue(all(a1_attr <= a2_attr for a1_attr, a2_attr in attrs) or
                                all(a1_attr >= a2_attr for a1_attr, a2_attr in attrs))

        self.assertEqual([a.name for a in self.achs], [
            'Explorador principiante',
            'Escudero estelar',
            'Cadete espacial',
            'Explorador habilidoso',
            'Observador nato',
            'Cosmonauta destacado',
            'Corsario galáctico',
            'Explorador estelar de élite',
            'Conquistador galáctico',
            'Emperador galáctico',
        ])
