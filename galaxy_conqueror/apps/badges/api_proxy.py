import os
import json
import requests
from django.conf import settings
from django.utils.timezone import now
from notifications.models import Notification


class ApiProxy(object):

    URL = 'https://cientopolis.lifia.info.unlp.edu.ar/badges-api/'

    def __init__(self, testing_mode):
        self.app_url = ApiProxy.URL + 'issuers/%s/' % settings.BADGES_API_APP_ID

    def get_badges(self):
        return requests.get(ApiProxy.URL + 'badges', verify=False).json()

    def get_achievements(self):
        return requests.get(self.app_url + 'badges', verify=False).json()

    def badge_is_own_by_user(self, badge_id, user_email):
        response = requests.get(ApiProxy.URL + 'badges/%s/instances/%s' % (badge_id, user_email))
        return response.json()

    def achievement_is_own_by_user(self, badge_id, user_email):
        t = badge_id, user_email
        response = requests.get(ApiProxy.APP_URL + 'badges/%s/instances/%s' % t)
        return response.json()

    def get_badges_by_user(self, user_email):
        return requests.get(ApiProxy.URL + 'instances/%s' % user_email, verify=False).json()

    def get_achievements_by_user(self, user_email):
        return requests.get(ApiProxy.APP_URL + 'instances/%s' % user_email, verify=False).json()

    def create_badge_acquisition(self, badge_id, user_email):
        return requests.post(ApiProxy.URL + 'badges/%s/instances' % badge_id, data={
            'email': user_email,
        }).json()

    def create_achievement_acquisition(self, badge_id, user_email):
        return requests.post(ApiProxy.APP_URL + 'badges/%s/instances' % badge_id, data={
            'email': user_email,
        }).json()


class FakeJsonApiProxy(ApiProxy):

    achievements_json_filename = settings.BASE_DIR + '/badges.json'

    def __init__(self):
        self.user_achievements_json_filename = (
            '%s/user_achievements/%s.json' % (os.path.dirname(__file__), settings.BADGES_API_APP_ID)
        )

    def reset(self):
        with open(self.user_achievements_json_filename, 'w') as f:
            json.dump([], f)

    def get_badges(self):
        return []

    def badge_is_own_by_user(self, badge_id, user_email):
        return {
            'issuedOn': None,
        }

    def get_badges_by_user(self, user_email):
        return []

    def get_achievements(self):
        with open(self.achievements_json_filename) as f:
            achievements = json.load(f)
            achievements.sort()
            for i, a in enumerate(achievements):
                a['id_badge_class'] = 'fake_id_%d' % i
            return achievements

    def achievement_is_own_by_user(self, badge_id, user_email):
        for ac in self.get_achievements_by_user(user_email)[user_email]:
            if ac['id_badge_class'] == badge_id:
                return {
                    'issuedOn': ac['issuedOn'],
                }

        return {
            'issuedOn': None,
        }

    def get_achievements_by_user(self, user_email):
        achievements = self.get_achievements()
        with open(self.user_achievements_json_filename, 'r') as f:
            instances = json.load(f)
            instances_by_id = {ac['id_badge_class']: ac
                               for ac in instances
                               if ac['user_email'] == user_email}

            return {
                user_email: [{
                    'id_badge_class': a['id_badge_class'],
                    'name': a['name'],
                    'issuedOn': instances_by_id[a['id_badge_class']]['timestamp'],
                } for a in achievements if a['id_badge_class'] in instances_by_id],
            }

    def create_badge_acquisition(self, badge_id, user_email):
        raise NotImplementedError()

    def create_achievement_acquisition(self, badge_id, user_email):
        new_acquisition = {
            'id_badge_class': badge_id,
            'user_email': user_email,
            'timestamp': now().strftime('%Y-%m-%d')
        }
        if not os.path.isfile(self.user_achievements_json_filename):
            self.reset()
        with open(self.user_achievements_json_filename, 'r+') as f:
            acquisitions = json.load(f)
            acquisitions.append(new_acquisition)
            f.seek(0)
            f.truncate()
            json.dump(acquisitions, f)

        return {
            'issuedOn': new_acquisition['timestamp'],
        }

proxy = FakeJsonApiProxy()
