from django.conf.urls import patterns, url

urlpatterns = patterns('badges.views', *[

    url(r'^(?P<user_id>\d+)/(?P<user_slug>[\w-]+)/(?P<badge_slug>[\w-]+)/$',
        'show_achievement',
        name='show_achievement'),

    url(r'^images/(?P<badge_slug>[\w-]+).png$',
        'image',
        name='badge_image'),

])
