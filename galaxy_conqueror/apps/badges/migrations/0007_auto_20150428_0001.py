# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os

from django.conf import settings
from django.db import migrations
from django.core.management import call_command


def load_inital_data_from_fixture(apps, schema_editor):
    call_command('loaddata', os.path.join(
        settings.BASE_DIR,
        'galaxy_conqueror/apps/badges/fixtures/initial_data.json',
    ))


def clear(apps, schema_editor):
    apps.get_model("badges", "Badge").objects.all().delete()


class Migration(migrations.Migration):

    dependencies = [
        ('badges', '0006_badge_message'),
    ]

    operations = [
        migrations.RunPython(load_inital_data_from_fixture, clear),
    ]
