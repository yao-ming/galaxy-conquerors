# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


def copy_desc_to_message(apps, schema_editor):

    for badge in apps.get_model("badges", "Badge").objects.all():
        badge.message = badge.description
        badge.description = ''
        badge.save()


class Migration(migrations.Migration):

    dependencies = [
        ('badges', '0005_auto_20150331_1409'),
    ]

    operations = [
        migrations.AddField(
            model_name='badge',
            name='message',
            field=models.TextField(default=''),
            preserve_default=False,
        ),
        migrations.RunPython(copy_desc_to_message),
    ]
