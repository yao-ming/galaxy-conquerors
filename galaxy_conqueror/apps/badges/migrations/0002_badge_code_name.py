# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('badges', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='badge',
            name='code_name',
            field=models.CharField(default='unnamed_badge', unique=True, max_length=100),
            preserve_default=False,
        ),
    ]
