# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('badges', '0003_badge_amount_required'),
    ]

    operations = [
        migrations.AddField(
            model_name='badge',
            name='category',
            field=models.CharField(default='h', max_length=1, choices=[(b'h', 'Insignia de cazador')]),
            preserve_default=False,
        ),
    ]
