from .models import Badge

ACHIEVEMENTS_EXTRA_DATA = {
    u"Libertad de expresión estelar": {
        'category': Badge.CITIZEN_BADGES_CATEGORY,
        'required_amount': 1,
        'message': u"¡Felicitaciones! Has ejercido tu derecho al sufragio por primera vez"
    },
    u"Colaborador comunitario": {
        'category': Badge.CITIZEN_BADGES_CATEGORY,
        'required_amount': 5,
        'message': u"¡Felicitaciones! Ya has votado 5 veces",
    },
    u"Militante galáctico": {
        'category': Badge.CITIZEN_BADGES_CATEGORY,
        'required_amount': 15,
        'message': u"¡Felicitaciones! Ya has votado 15 veces",
    },
    u"Senador galáctico": {
        'category': Badge.CITIZEN_BADGES_CATEGORY,
        'required_amount': 30,
        'message': u"¡Felicitaciones! Ya has votado 30 veces",
    },
    u"Jugador destacado": {
        'category': Badge.EXPLORER_BADGES_CATEGORY,
        'required_amount': 1,
        'message': u"¡Felicitaciones! Has encontrado tu primer galaxia",
    },
    u"Vista de águila": {
        'category': Badge.EXPLORER_BADGES_CATEGORY,
        'required_amount': 2,
        'message': u"¡Felicitaciones! Has encontrado tu segunda galaxia",
    },
    u"Vista telescópica": {
        'category': Badge.EXPLORER_BADGES_CATEGORY,
        'required_amount': 4,
        'message': u"¡Felicitaciones! Has encontrado tu cuarta galaxia",
    },
    u"Explorador estelar de élite": {
        'category': Badge.EXPLORER_BADGES_CATEGORY,
        'required_amount': 5,
        'message': u"¡Felicitaciones! Has encontrado tu quinta galaxia",
    },
    u"Cazador principiante": {
        'category': Badge.HUNTER_BADGES_CATEGORY,
        'required_amount': 1,
        'message': u"¡Felicitaciones! Has capturado tu primer alien",
    },
    u"Cazador habilidoso": {
        'category': Badge.HUNTER_BADGES_CATEGORY,
        'required_amount': 50,
        'message': u"¡Felicitaciones! Ya llevas 50 aliens capturados",
    },
    u"Cazador experto": {
        'category': Badge.HUNTER_BADGES_CATEGORY,
        'required_amount': 100,
        'message': u"¡Felicitaciones! Ya has capturado 100 aliens",
    },
    u"El terror de los aliens": {
        'category': Badge.HUNTER_BADGES_CATEGORY,
        'required_amount': 500,
        'message': u"¡Felicitaciones! Ya has capturado 500 aliens",
    },
    u"Cadete espacial": {
        'category': Badge.PLAYER_BADGES_CATEGORY,
        'required_amount': 1,
        'message': u"¡Bienvenido a Galaxy Conqueror!",
    },
    u"Astrónomo nato": {
        'category': Badge.PLAYER_BADGES_CATEGORY,
        'required_amount': 100,
        'message': u"¡Felicitaciones! Has obtenido tus primeros 100 puntos",
    },
    u"Corsario galáctico": {
        'category': Badge.PLAYER_BADGES_CATEGORY,
        'required_amount': 500,
        'message': u"¡Felicitaciones! Has obtenido tus primeros 500 puntos",
    },
    u"Conquistador galáctico": {
        'category': Badge.PLAYER_BADGES_CATEGORY,
        'required_amount': 1000,
        'message': u"¡Felicitaciones! Has obtenido tus primeros 1000 puntos",
    },
}
