from django.conf.urls import patterns, url


urlpatterns = patterns('reports.views', *[
    url(r'^$', 'report', name='report'),
    # url(r'^heatmap/$', 'heatmap', name='heatmap'),
    url(r'^confirm-galaxy/(?P<galaxy_id>\d+)/$', 'confirm_galaxy', name='confirm_galaxy'),
    url(r'^reject-galaxy/(?P<galaxy_id>\d+)/$', 'reject_galaxy', name='reject_galaxy'),
])
