from datetime import datetime
from .models import User, UserActivity, ActivityType


def log_user_activity(backend, details, response, user=None, is_new=False, *args, **kwargs):

    if isinstance(user, User):

        user_activity = UserActivity()
        user_activity.user = user
        user_activity.activity_type = ActivityType.login
        user_activity.save()


def save_profile(backend, user, response, *args, **kwargs):
    if backend.name == 'facebook':
        profile = user
        if profile is None:
            profile = User(user_id=user.id)
    if response.get('email'):
        profile.email = response.get('email')
    else:
        profile.email = response.get('id') + '@esuniddefacebook.com'
    profile.timezone = response.get('timezone')
    profile.save()
