from django.conf.urls import patterns, url


urlpatterns = patterns('users.views', *[
    url(r'^$', 'login', name='login'),
    url(r'^profile/', 'profile', name='profile'),
    url(r'^logout/', 'logout', name='logout'),
    url(r'^signup/', 'signup', name='signup'),
])
