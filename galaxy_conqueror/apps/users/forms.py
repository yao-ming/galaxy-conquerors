# -*- coding: utf-8 -*-
from django import forms
from users.models import User
from django.contrib.auth.forms import AuthenticationForm


class LoginForm(AuthenticationForm):
    username = forms.CharField(label="Nombre de usuario", max_length=254)
    password = forms.CharField(label="Contraseña", widget=forms.PasswordInput)


class SignUpForm(forms.Form):
    username = forms.CharField()
    email = forms.EmailField()
    password_1 = forms.CharField(widget=forms.PasswordInput())
    password_2 = forms.CharField(widget=forms.PasswordInput())

    def clean_email(self):
        email = self.cleaned_data.get("email")

        try:
            User.objects.get(email=email)
        except User.DoesNotExist:
            return email

        raise forms.ValidationError("Ya existe un usuario con ese email")

    def clean_username(self):
        username = self.cleaned_data.get("username")

        try:
            User.objects.get(username=username)
        except User.DoesNotExist:
            return username

        raise forms.ValidationError("Ya existe un usuario con ese username")

    def clean_password_2(self):
        password1 = self.cleaned_data.get("password_1")
        password2 = self.cleaned_data.get("password_2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(u"Las constraseñas no coinciden")
        return password2

    def save(self):
        user = User()
        user.username = self.cleaned_data["username"]
        user.email = self.cleaned_data["email"]
        user.set_password(self.cleaned_data["password_1"])
        return user.save()
