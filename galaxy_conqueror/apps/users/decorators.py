from .models import UserActivity, ActivityType


def log_user_activity(some_view):

    def wrapped_view(request, *args, **kwargs):
        # this code assumes that the name of the ActivityType value corresponds to the name of
        # the decorated view
        user_activity = UserActivity()
        user_activity.user = request.user
        user_activity.activity_type = ActivityType[some_view.__name__]
        user_activity.save()
        return some_view(request, *args, **kwargs)

    return wrapped_view
