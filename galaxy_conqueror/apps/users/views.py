from django.contrib import messages
from django.shortcuts import render, redirect
from django.contrib.auth.views import logout_then_login
from users.forms import SignUpForm, LoginForm
from django.contrib.auth.forms import AuthenticationForm
from django.contrib import auth

def signup(request):
    form = SignUpForm(request.POST or None)
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            messages.success(request, "Su cuenta ha sido creada con exito")
            return redirect('login')
    return render(request, 'users/signup.jinja', {
        'form': form,
    })


def login(request):
    form = AuthenticationForm(data=request.POST)
    if request.method == 'POST':
        if form.is_valid():
            auth.login(request, form.get_user())
            return redirect('/play/')
    return render(request, 'users/login.jinja', {
        'form': form,
    })


def profile(request):
    return render(request, 'users/profile.jinja', {
        'user': request.user,
    })


def logout(request):
    return logout_then_login(request)
