import datetime

from django.db import models
from users.models import User


class TutorialStats(models.Model):

    user = models.OneToOneField(User, related_name='tutorial_stats')
    did_tutorial = models.BooleanField(default=False)
    rejected_to_take_tutorial = models.BooleanField(default=False)
    tutorial_rejected_last_at = models.DateTimeField(null=True, blank=True)

    def should_take_tutorial(self):
        if self.did_tutorial:
            return False
        if not self.rejected_to_take_tutorial:
            return True
        return self.tutorial_rejected_last_at.date() != datetime.date.today()

    def reject(self):
        self.rejected_to_take_tutorial = True
        self.tutorial_rejected_last_at = datetime.datetime.now()
        self.save()

    def done(self):
        self.did_tutorial = True
        self.save()
