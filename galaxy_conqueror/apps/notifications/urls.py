from django.conf.urls import patterns, include, url


urlpatterns = patterns('notifications.views', *[
    url(r'^$', 'mark_as_read', name='mark_notifications_as_read'),
])