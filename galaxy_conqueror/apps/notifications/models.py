# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from users.models import User


class Notification(models.Model):

    user = models.ForeignKey(User)
    timestamp = models.DateTimeField(auto_now_add=True)
    content = models.TextField()
    notified = models.BooleanField(default=False)
    fb_feed_title = models.CharField(max_length=64, blank=True, null=True)
    fb_feed_description = models.TextField(blank=True, null=True)

    class Meta:
        ordering = ('timestamp',)

    @classmethod
    def create_achievement_notification(cls, achievement, user):
        cls.objects.create(
            user=user,
            content="¡Felicitaciones! Has obtenido una nueva insignia: %s" % achievement.name,
            fb_feed_title="¡%s!" % achievement.name,
            fb_feed_description=achievement.description_for_fb_feed,
        )

    @classmethod
    def unread_for_user(cls, user):
        return cls.objects.filter(user=user, notified=False)

    @classmethod
    def unread_for_user_as_dicts(cls, user):
        return [n.as_dict() for n in cls.unread_for_user(user)]

    def __unicode__(self):
        return self.content

    def as_dict(self):
        return {
            'id': self.id,
            'content': self.content,
            'fb_feed_title': self.fb_feed_title,
            'fb_feed_description': self.fb_feed_description,
        }
