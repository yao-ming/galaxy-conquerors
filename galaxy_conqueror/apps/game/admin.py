# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import GalaxyCandidate, Vote


def confirm_selected_galaxy_candidates(modeladmin, request, queryset):
    for galaxy_candidate in queryset:
        galaxy_candidate.confirm(request.user)


def reject_selected_galaxy_candidates(modeladmin, request, queryset):
    for galaxy_candidate in queryset:
        galaxy_candidate.reject(request.user)


class GalaxyCandidateModelAdmin(admin.ModelAdmin):
    list_display = (
        'img',
        'timestamp',
        'coordinates',
        'owner_displayed_name',
        'status',
        'staff_votes',
        'votes_count',
    )
    actions = [confirm_selected_galaxy_candidates, reject_selected_galaxy_candidates]

    def staff_votes(self, galaxy_candidate):
        def as_p(vote):
            return u'<li style="color: %s">%s says this is %s </li>' % (
                'green' if vote.is_positive else 'red',
                vote.owner.displayed_name(),
                'a galaxy' if vote.is_positive else 'not a galaxy'
            )
        staff_votes = galaxy_candidate.votes.filter(owner__is_staff=True)
        return "<ul>%s</ul>" % ''.join(as_p(vote) for vote in staff_votes)
    staff_votes.allow_tags = True

    def owner_displayed_name(self, galaxy_candidate):
        return galaxy_candidate.owner.displayed_name()
    owner_displayed_name.short_description = 'Owner'
    owner_displayed_name.admin_order_field = 'owner'

    def coordinates(self, galaxy_candidate):
        return u'(%.2f, %.2f)' % (galaxy_candidate.x, galaxy_candidate.y)

    def img(self, galaxy_candidate):
        return u'<img src="%s" />' % (galaxy_candidate.thumbnail_url())
    img.short_description = 'Thumbnail'
    img.allow_tags = True

    def votes_count(self, galaxy_candidate):
        votes = galaxy_candidate.votes.filter(owner__is_staff=False)
        total = votes.count()
        if total == 0:
            return ""
        positives = votes.filter(is_positive=True).count()
        negatives = total - positives
        positives_percent = float(positives) / total * 100
        negatives_percent = float(negatives) / total * 100
        return "positives: %d (%%%.2f), negatives: %d (%%%.2f), total: %d" % (
            positives,
            positives_percent,
            negatives,
            negatives_percent,
            total,
        )

admin.site.register(GalaxyCandidate, GalaxyCandidateModelAdmin)
admin.site.register(Vote)
