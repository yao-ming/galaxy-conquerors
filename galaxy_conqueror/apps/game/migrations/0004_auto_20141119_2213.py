# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('game', '0003_auto_20141116_1825'),
    ]

    operations = [
        migrations.AlterField(
            model_name='alien',
            name='image',
            field=models.ImageField(upload_to=b'aliens'),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='vote',
            unique_together=set([('owner', 'galaxy_candidate')]),
        ),
    ]
