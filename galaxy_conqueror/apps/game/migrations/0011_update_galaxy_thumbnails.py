# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import transaction
from django.db import migrations
from game.helpers import create_galaxy_thumbnail


@transaction.atomic
def update_galaxy_thumbnails(apps, schema_editor):
    # We can't import the GalaxyCandidate model directly as it may be a newer
    # version than this migration expects. We use the historical version.
    GalaxyCandidate = apps.get_model("game", "GalaxyCandidate")
    for galaxy in GalaxyCandidate.objects.all():
        url = create_galaxy_thumbnail(galaxy)
        galaxy.thumbnail.name = url
        galaxy.save()


class Migration(migrations.Migration):

    dependencies = [
        ('game', '0010_galaxycandidate_thumbnail'),
    ]

    operations = [
        migrations.RunPython(update_galaxy_thumbnails),
    ]
