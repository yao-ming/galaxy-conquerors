# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('game', '0006_remove_galaxycandidate_confirmed'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='badge',
            name='owners',
        ),
        migrations.DeleteModel(
            name='Badge',
        ),
    ]
