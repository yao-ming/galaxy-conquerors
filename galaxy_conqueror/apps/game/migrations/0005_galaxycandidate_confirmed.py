# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('game', '0004_auto_20141119_2213'),
    ]

    operations = [
        migrations.AddField(
            model_name='galaxycandidate',
            name='confirmed',
            field=models.NullBooleanField(),
            preserve_default=True,
        ),
    ]
