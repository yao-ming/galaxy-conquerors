# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('game', '0007_auto_20150317_1213'),
    ]

    operations = [
        migrations.CreateModel(
            name='AlienHunted',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('x', models.DecimalField(max_digits=15, decimal_places=2)),
                ('y', models.DecimalField(max_digits=15, decimal_places=2)),
                ('timestamp', models.DateTimeField(auto_now_add=True)),
                ('hunter', models.ForeignKey(related_name='hunted_aliens', to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
