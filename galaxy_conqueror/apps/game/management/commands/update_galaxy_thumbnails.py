import json
import requests
from optparse import make_option
from django.core.management.base import BaseCommand
from game.models import GalaxyCandidate


class Command(BaseCommand):

    option_list = BaseCommand.option_list + (
        make_option('--all',
            action='store_true',
            help='Update thumbnails for all galaxies, even those which already have one'),
    )

    def handle(self, *args, **options):
        must_update_all = options.get('all', False)
        gs = GalaxyCandidate.objects.all()

        if must_update_all:
            total = GalaxyCandidate.objects.count()
        else:
            galaxies_without_thumbnail = []
            for g in gs:
                if not g.thumbnail or not g.thumbnail.name:
                    galaxies_without_thumbnail.append(g)
            gs = galaxies_without_thumbnail
            total = len(gs)
        counter = 0
        self.stdout.write("Updating thumbnails on %d galaxy candidates" % total)
        for g in gs:
            counter += 1
            if counter % 10 == 0:
                self.stdout.write("%.2f%%" % (counter / float(total) * 100))
            g.update_thumbnail()
        self.stdout.write("100%")
