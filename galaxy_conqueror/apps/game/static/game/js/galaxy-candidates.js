/*globals
    jQuery,
    _,
    ol,
    data,
    window,
    flagImages,
*/

var galaxyCandidates = (function ($, _, ol, data, setTimeout) {

    'use strict';

    var templates,
        popupOverlay,
        popupContainer,
        popupContent,
        map,
        imagesUrl,
        flagsOverlay,
        currentGalaxyId;

    // galaxt candidate features layer--------------------------------------------------------------
    function buildGalaxyCandidateFeature(coords, id) {
        return new ol.Feature({
            geometry: new ol.geom.Point(coords),
            size: flagImages.size,
            type: 'flag',
            cursor: 'pointer',
            id: id,
        });
    }

    function buildFlagsOverlay() {
        var source = new ol.source.Vector();

        source.addFeatures($.map(data.getAllGalaxyCandidates(), function (candidate) {
            return buildGalaxyCandidateFeature([candidate.x, candidate.y], candidate.id);
        }));

        return new ol.layer.Vector({
            source: source,
            style: function (feature) {
                var properties = feature.getProperties(),
                    galaxy = data.getGalaxyCandidate(properties.id),
                    currentUserVoteStatus,
                    expertVoteStatus,
                    src;

                if (galaxy.isConfirmed) {
                    expertVoteStatus = 'confirmed';
                } else if (galaxy.isRejected) {
                    expertVoteStatus = 'rejected';
                } else {
                    expertVoteStatus = 'unknown';
                }

                if (galaxy.currentUserVote === true) {
                    currentUserVoteStatus = 'positive';
                } else if (galaxy.currentUserVote === false) {
                    currentUserVoteStatus = 'negative';
                } else {  // null
                    currentUserVoteStatus = 'unknown';
                }

                src = imagesUrl + flagImages.filenames[currentUserVoteStatus][expertVoteStatus];

                return [new ol.style.Style({
                    image: new ol.style.Icon({
                        anchor: flagImages.anchor,
                        size: [properties.size[0], properties.size[1]],
                        scale: 1,
                        src: src,
                    })
                })];
            }
        });
    }

    // popup----------------------------------------------------------------------------------------
    function voteHandlerFunction(isPositive) {
        return function () {
            data.vote(currentGalaxyId, isPositive, function () {
                var galaxy = data.getGalaxyCandidate(currentGalaxyId);
                popupContent.innerHTML = templates.popup(galaxy);
                flagsOverlay.changed();
                incrementVoteCountSEUO()
            });
        };
    }

    function incrementVoteCountSEUO() {
        var $voteCount = $($('.player-stats li')[1]).find('span');
        $voteCount.text((parseInt($voteCount.text())+1).toString());
    }

    function discussHandlerFunction() {
        return function () {
            FB.ui({
                method: 'feed',
                name: "¿Es una galaxia?",
                description: "Alguien (tal vez yo) dijo que el objeto luminoso en el centro de esta imagen" +
                " es una galaxia ¿A vos que te parece? ¿Voto a favor o en contra? Mencioná @Cientópolis en tu post " +
                "para involucrar a los expertos en la discusión.",
                link: 'https://galaxyconqueror.cientopolis.org/',
                caption: 'Galaxy Conqueror - Cientopolis',
                picture: 'https://galaxyconqueror.cientopolis.org/media/galaxy-thumbnails/' + currentGalaxyId.toString() + '.jpeg',
            });
        };
    }


    function openGalaxyPopup(feature) {
        currentGalaxyId = feature.getProperties().id;
        var coord = feature.getGeometry().getCoordinates(),
            resolution = map.getView().getResolution(),
            galaxy = data.getGalaxyCandidate(currentGalaxyId);
        coord[0] = (parseInt(coord[0], 10) - 6 * resolution).toString();
        coord[1] = (parseInt(coord[1], 10) + 45 * resolution).toString();
        popupOverlay.setPosition(coord);
        popupContent.innerHTML = templates.popup(galaxy);
    }

    function closeGalaxyPopup() {
        popupOverlay.setPosition(undefined);
    }

    function buildPopupOverlay() {
        popupContainer = $('#popup')[0];
        popupContent = $('#popup-content')[0];
        $(popupContent).on('click', '.votes .positive', voteHandlerFunction(true));
        $(popupContent).on('click', '.votes .negative', voteHandlerFunction(false));
        $(popupContent).on('click', '.votes .discuss', discussHandlerFunction());
        return new ol.Overlay({
            element: popupContainer,
            autoPan: true,
            autoPanAnimation: {
                duration: 250
            }
        });
    }


    // add galaxy-----------------------------------------------------------------------------------
    function addGalaxyCandidateAt(coords) {
        var galaxyCandidateId = data.addGalaxyCandidate(coords);
        flagsOverlay.getSource().addFeature(buildGalaxyCandidateFeature(coords, galaxyCandidateId));
    }


    return {
        init: function (params) {

            imagesUrl = params.imagesUrl;
            flagImages.init({
                imagesUrl: imagesUrl,
            });

            currentGalaxyId = null;

            map = params.map;

            flagsOverlay = buildFlagsOverlay();
            popupOverlay = buildPopupOverlay();

            templates = {
                popup: _.template($("#popup-template").html()),
            };

            map.addOverlay(popupOverlay);
            map.addOverlay(flagsOverlay);

            return this;
        },
        addGalaxyCandidateAt: addGalaxyCandidateAt,
        openGalaxyPopup: openGalaxyPopup,
        closeGalaxyPopup: closeGalaxyPopup,
    };
}(jQuery, _, ol, data, window.setTimeout));