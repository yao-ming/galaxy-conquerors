/*globals
    jQuery,
    ol,
*/

var controls = (function ($, ol) {

    'use strict';

    function MarkGalaxyControl(opt_options) {
        var options = opt_options || {};
        var $button = $('<button>');
        $button.append($('<div>').addClass('fa fa-crosshairs'));
        $button.attr("title","Proponer galaxia")
        var $div = $('<div>')
                .addClass('markGalaxy')
                .addClass('ol-control')
                .append($button);

        $button.on('click', options.onClick);
        ol.control.Control.call(this, {
            element: $div[0],
            target: options.target
        });
    }
    ol.inherits(MarkGalaxyControl, ol.control.Control);


     function TutorialControl(opt_options) {
        var options = opt_options || {};
        var $button = $('<button>');
        $button.attr("title","Abrir el tutorial")
        $button.append($('<div>').addClass('fa fa-info'));
        var $label = $button.attr('id', 'open-tutorial-btn');
        var $div = $('<div>').addClass('open-tutorial-btn')
                .addClass('open-tutorial-btn')
                .addClass('ol-control')
                .append($label);
        $button.on('click', options.onClick);
        ol.control.Control.call(this, {
            element: $div[0],
            target: options.target
        });
    }
    ol.inherits(TutorialControl, ol.control.Control);

    function SoundControl(opt_options) {
        var options = opt_options || {};
        var $button = $('<button>');
        $button.attr("title","Activar/desactivar la música")
        var $icon = $('<div>').addClass('fa');
        var sound = document.getElementById("background-sound");
        $icon.toggleClass("fa-volume-up", ! sound.paused);
        $icon.toggleClass("fa-volume-off", sound.paused);

        $button.append($icon);
        var $label = $button.attr('id', 'toggle-sound-btn');
        var $div = $('<div>')
                .addClass('toggle-sound-btn')
                .addClass('ol-control')
                .append($label);
        $button.on('click', options.onClick);
        ol.control.Control.call(this, {
            element: $div[0],
            target: options.target
        });
    }
    ol.inherits(SoundControl, ol.control.Control);


    function ShareControl(opt_options) {
        var options = opt_options || {};
        var $button = $('<button>');
        $button.attr("title","Compartir en Facebook")
        $button.append($('<div>').addClass('fa fa-share-alt'));
        var $label = $button.attr('id', 'share-btn');
        var $div = $('<div>').addClass('share-btn')
                .addClass('ol-control')
                .append($label);
        $button.on('click', options.onClick);
        ol.control.Control.call(this, {
            element: $div[0],
            target: options.target
        });
    }
    ol.inherits(ShareControl, ol.control.Control);

    return {
        MarkGalaxyControl: MarkGalaxyControl,
        TutorialControl: TutorialControl,
        SoundControl: SoundControl,
        ShareControl: ShareControl,
    };

}(jQuery, ol));