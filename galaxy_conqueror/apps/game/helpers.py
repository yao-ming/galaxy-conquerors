from PIL import Image
from django.conf import settings


def get_galaxy_coordinates_at_image(galaxy):
    # for ol3, coords in the image are centered in 0, 0
    # valid coordinates lies inside this extent:
    # [- max_coord, - max_coord, max_coord, max_coord]
    max_coord = 20037508.342789244

    # the original image is not square. a light blue strip both top and bottom sides is added
    # because of that, the left coorner of the image is a bit lower than
    # (- max_coord, max_coord)
    left_corner = complex(- max_coord, max_coord - 332653.9470970854)

    # Pillow pixel coordinates sets (0, 0) on the left top corner
    image_pixels_size = 6101, 6000

    coords_per_pixel = max_coord * 2 / image_pixels_size[0]

    point = complex(galaxy.x, galaxy.y)

    # lets use the ol3 coordinates, as if the left top corner where the origin
    point -= left_corner
    # right now the imaginary component is negative, so lets use the conjugate
    point = point.conjugate()
    # left transform the coordinates into pixels
    point /= coords_per_pixel
    point = complex(
        round(point.real),
        round(point.imag),
    )

    return point


def create_galaxy_thumbnail(galaxy):
    input_filename = settings.PROJECT_DIR + '/apps/game/static/game/img/ngc2467.jpeg'
    output_filename = '/galaxy-thumbnails/%d.jpeg' % galaxy.id
    output_url = '/media' + output_filename
    output_path = settings.MEDIA_ROOT + output_filename

    point = get_galaxy_coordinates_at_image(galaxy)
    crop_size = 50

    box = [
        int(point.real) - crop_size / 2,
        int(point.imag) - crop_size / 2,
        int(point.real) + crop_size / 2,
        int(point.imag) + crop_size / 2,
    ]

    with Image.open(input_filename) as image:
        region = image.crop(box)
        # from PIL import ImageDraw
        # draw = ImageDraw.Draw(region)
        # draw.line((0, 0) + region.size, fill=128)
        # draw.line((0, region.size[1], region.size[0], 0), fill=128)
        region.save(output_path)

    return output_url
