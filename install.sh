sudo apt-get install python-pip
sudo pip install virtualenv
sudo pip install virtualenvwrapper
mkdir ~/envs
echo "export WORKON_HOME=$HOME/envs" >> ~/.bashrc
echo "export PROJECT_HOME=$HOME/Devel" >> ~/.bashrc
echo "source /usr/local/bin/virtualenvwrapper.sh"  >> ~/.bashrc
source ~/.bashrc
mkvirtualenv GC
workon GC
pip install -r requirements.txt
python manage.py migrate
